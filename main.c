#include <stdio.h>
#include <string.h>
 
//constantes
 
#define MAX_DIARIO 5
#define MAX_DESIGNACAO 51
 
#define MAX_TRATAMENTO 30
#define MAX_UTENTE 100
#define MAX_CLINICA 50
#define MAX_TRATAMENTOS_DISPONIVEIS 20
#define MAX_ANO 365
 
//estrututas
typedef struct{ //data
 
int dia;
int mes;
int ano;
 
}tdata;
 
typedef struct{ // marcação (dados requeridos para agendar tratamentos)
 
    int numero_utente;
    int numero_tratamento;
    int realizada;
 
}tMarcacao;
 
typedef struct{ //agenda (numero de marcações por dia)
    int nmarcacoes;
    tdata data;
    tMarcacao marcacao [MAX_DIARIO];
}tAgenda;
 
typedef struct{ //clinicas
 
    char designacao [MAX_DESIGNACAO];
    char localidade [MAX_DESIGNACAO];
    int  tratamentosDisponiveis [MAX_TRATAMENTOS_DISPONIVEIS] ;
    int ntratamentos;
    tAgenda agenda [MAX_ANO];
    int nAgenda;
 
} tClinica;
 
typedef struct { //utentes
 
int numero;
char nome [MAX_DESIGNACAO];
tdata data;
 
}tUtente;
 
typedef struct{ //tratamento
 
int numero;
char designacao [MAX_DESIGNACAO];
float custo;
 
} tTratamento;
 
//---------------funções------------------------
 
//verificações
int lerInteiro(int min,int max);
void lerString(char str[], int max);
 
//menus
int menu(int nClinicas, int nutentes);
int menuUtentes();
int menuClinicas();
int menuTratamentos();
 
 
//utentes
void adicionarUtente(tUtente utentes[], int *nutentes);
int alterarUtente(tUtente utentes[], int *nutentes);
void mostrarUtente(tUtente utentes[], int *nutentes);
void eliminarUtente(tUtente utentes[], int *nUtentes);
 
void escreverFicheiroBinarioUtente(tUtente utentes[], int nUtentes);
void lerFicheiroBinarioUtente(tUtente utentes[], int *nUtentes);
 
 
//tratamentos
void inserirTratamento(tTratamento tratamentos[], int *nTratamentos);
int alterarTratamento(tTratamento tratamentos[], int *nTratamentos, tClinica clinicas[], int *nClinicas);
void mostrarTratamento(tTratamento tratamentos[], int *nTratamentos);
 
void escreverFicheiroBinarioTratamento(tTratamento tratamentos[],int nTratamentos);
void lerFicheiroBinarioTratamento(tTratamento tratamentos[],int *nTratamentos);
 
 
//clinicas
void inserirClinica(tClinica clinicas[], int *nClinica);
int alterarClinica(tClinica clinicas[], int *nClinica);
void eliminarClinica(tClinica clinicas[], int *nClinicas);
void mostrarClinica(tClinica clinicas[], int *nClinicas);
 
int associarTratamentos (tClinica clinicas[], int *nClinicas, tTratamento tratamentos[], int *nTratamentos);
int mostrarTratamentosClinica (tClinica clinicas[], int *nClinicas, tTratamento tratamentos[], int *nTratamentos);
 
void escreverFicheiroBinarioClinica(tClinica clinicas[],int nClinicas);
void lerFicheiroBinarioClinica(tClinica clinicas[],int *nClinicas);
 
 
//---------inicio do programa-----------------------
 
int main(){
 
tTratamento tratamentos [MAX_TRATAMENTO];
tClinica clinicas [MAX_CLINICA];
tUtente utentes [MAX_UTENTE];
int nTratamentos=0, nClinicas=0, nUtentes=0;
int opcao, opcao2;
 
// chamada função para ler dados ficheiro
lerFicheiroBinarioUtente(utentes, &nUtentes);
lerFicheiroBinarioClinica(clinicas, &nClinicas);
lerFicheiroBinarioTratamento(tratamentos,&nTratamentos);
 
do{
 
opcao = menu(nClinicas,nUtentes);
switch (opcao){
                case 1: //função
                    opcao2 = menuUtentes();
 
                    switch(opcao2){
                                    case 1:
                                        adicionarUtente(utentes, &nUtentes);
                                        break;
 
                                    case 2:
                                        alterarUtente(utentes, &nUtentes);
                                        break;
 
                                    case 3:
                                        eliminarUtente(utentes, &nUtentes);
                                        break;
 
                                    case 4:
                                        mostrarUtente(utentes, &nUtentes);
                                        break;
                    }
                    break;
 
                case 2: //função
 
                    opcao2= menuClinicas();
                    switch(opcao2){
                                    case 1:
                                        inserirClinica(clinicas, &nClinicas);
                                        break;
 
                                    case 2:
                                        alterarClinica(clinicas, &nClinicas);
                                        break;
 
                                    case 3:
                                        eliminarClinica(clinicas, &nClinicas);
                                        break;
 
                                    case 4:
                                        mostrarClinica(clinicas, &nClinicas);
                                        break;
 
                                    case 5:
                                        associarTratamentos (clinicas, &nClinicas, tratamentos, &nTratamentos);
                                        break;
 
                                    case 6:
                                        mostrarTratamentosClinica (clinicas, &nClinicas, tratamentos, &nTratamentos);
                                        break;
 
                    }
                    break;
 
                case 3:
                 opcao2= menuTratamentos();
                    switch(opcao2){
                                    case 1:
                                         inserirTratamento(tratamentos, &nTratamentos);
                                        break;
 
                                    case 2:
                                        alterarTratamento(tratamentos, &nTratamentos, clinicas, &nClinicas);
                                        break;
 
                                    case 3:
                                        mostrarTratamento(tratamentos, &nTratamentos);
                                        break;
                    }
                    break;
            }
}while (opcao!=0);
 
//chamada função para escrever dados
escreverFicheiroBinarioUtente(utentes, nUtentes);
escreverFicheiroBinarioClinica(clinicas, nClinicas);
escreverFicheiroBinarioTratamento(tratamentos,nTratamentos);
return 0;
}
 
//---------------funções----------------
 
 
//verificações-----
 
int lerInteiro(int min,int max){
    int num;
    do
    {
        printf("Insira um valor inteiro (%d - %d): ",min,max);
        fflush(stdin);
        scanf("%d",&num);
    }
    while (num<min || num>max);
    return num;
}
 
void lerString(char str[], int max)
{
    do
    {
        fflush(stdin);
        fgets(str, max, stdin);
        str[strlen(str)-1] = '\0';
    }
    while(strlen(str)==0);
}
 
 
 
//menus---------
 
int menu(int nClinicas, int nutentes){
                system("cls");
 
            printf("\t\t\tCentro Tratamentos de Fisioterapia:\n\n\n");
 
            printf("\t  Numero de clinicas existentes: %d    Numero utentes: %d \n\n",nClinicas, nutentes);
            printf("\t  Numero de tratamentos agendados:     Receita Obtida:  \n\n\n");
 
            printf("\t1 - Menu de utentes\n\n");
            printf("\t2 - Menu de clinicas\n\n");
            printf("\t3 - Menu de tratamentos\n\n");
            printf("\t4 - Menu de agendamento de tatamentos\n\n");
 
            printf("\t0 - Sair\n");
            printf("\t\t\t\t\tEscolha a opcao desejada.\n");
int n=lerInteiro(0,4);
return n;
}
 
int menuUtentes(){
 
system("cls");
 
            printf("\t\t\tMenu dos utentes:\n\n\n");
            printf("\t1 - Adicionar Utente\n\n");
            printf("\t2 - Alterar utente\n\n");
            printf("\t3 - Eliminar Utente\n\n");
            printf("\t4 - Consultar utentes\n\n");
            printf("\t0 - Voltar\n");
            printf("\t\t\t\t\tEscolha a opcao desejada.\n");
int n=lerInteiro(0,4);
return n;
 
}
 
int menuClinicas(){
 
system("cls");
 
            printf("\t\t\tMenu das clinicas:\n\n\n");
            printf("\t1 - Adicionar Clinica\n\n");
            printf("\t2 - Alterar Clinica\n\n");
            printf("\t3 - Eliminar Clinica\n\n");
            printf("\t4 - Consultar Clinica\n\n");
            printf("\t5 - Associar tratamento\n\n");
            printf("\t6 - Mostrar Tratamentos disponiveis em uma Clinica\n\n");
            printf("\t0 - Voltar\n");
            printf("\t\t\t\t\tEscolha a opcao desejada.\n");
int n=lerInteiro(0,6);
return n;
 
}
 
int menuTratamentos(){
 
system("cls");
 
            printf("\t\t\tMenu dos Tratamentos:\n\n\n");
            printf("\t1 - Adicionar um Tratamento\n\n");
            printf("\t2 - Alterar um Tratamento\n\n");
            printf("\t3 - Consultar Tratamentos\n\n");
            printf("\t0 - Voltar\n");
            printf("\t\t\t\t\tEscolha a opcao desejada.\n");
int n=lerInteiro(0,3);
return n;
 
}
 
 
//funções para os utentes------------------
 
void adicionarUtente(tUtente utentes[], int *nutentes){
int i, numero;
 
if (*nutentes>=MAX_UTENTE){
    printf("\n Nao e possivel adicionar mais utentes. Elimine um antes de poder continuar.");
    getchar();getchar();
}
 
else{
    printf("\n\tInsira o numero do novo utente: ");
    //scanf("%d",&utentes[*nutentes].numero);
    do{
        scanf("%d",&numero);
        for (i=0;i<*nutentes;i++){
            if(utentes[i].numero==numero){
                printf("\nNumero ja existe. Insira outro numero: ");
                break;
            }
 
        }
 
    }while(utentes[i].numero==numero);
    utentes[*nutentes].numero=numero;
 
    printf("\n\tInsira o nome do novo utente: ");
    lerString(utentes[*nutentes].nome,MAX_DESIGNACAO);
 
    printf("\n\tDia da data de nascimento. ");
    //scanf("%d",&utentes[*nutentes].data.dia);
    utentes[*nutentes].data.dia=lerInteiro(1,31);
 
    printf("\n\tMes da data de nascimento. ");
    //scanf("%d",&utentes[*nutentes].data.mes);
    utentes[*nutentes].data.mes=lerInteiro(1,12);
 
    printf("\n\tAno da data de nascimento. ");
    //scanf("%d",&utentes[*nutentes].data.ano);
    utentes[*nutentes].data.ano=lerInteiro(1900,2100);
 
    *nutentes+=1;
    }
}
 
int alterarUtente(tUtente utentes[], int *nutentes){
int num_pesq, i, n, numero;
 
if (*nutentes==0){
    printf("\nNao existem utentes para poder alterar ");
    getchar();getchar();
    return 0;
}
 
printf("\nIndique o numero do utente que pretende alterar: ");
scanf("%d",&num_pesq);
 
for(i=0;i<*nutentes;i++){
    if (utentes[i].numero==num_pesq){
        printf("\n\n\tInsira o novo numero: ");
 
       do{
    scanf("%d",&numero);
    for (n=0;n<*nutentes;n++){
        if(utentes[n].numero==numero){
            printf("\nNumero ja existe. Insira outro numero: ");
            break;
        }
 
    }
 
}while(utentes[n].numero==numero);
utentes[i].numero=numero;
 
 
 
 
        printf("\n\tInsira o nome do novo utente: ");
        lerString(utentes[i].nome,MAX_DESIGNACAO);
 
        printf("\n\tDia da data de nascimento. ");
        //scanf("%d",&utentes[*nutentes].data.dia);
        utentes[i].data.dia=lerInteiro(1,31);
 
        printf("\n\tMes da data de nascimento. ");
        //scanf("%d",&utentes[*nutentes].data.mes);
        utentes[i].data.mes=lerInteiro(1,12);
 
        printf("\n\tAno da data de nascimento. ");
        //scanf("%d",&utentes[*nutentes].data.ano);
        utentes[i].data.ano=lerInteiro(1900,2100);
 
 
        return 0;
    }
 
}
printf("Nao foi encontrado o numero pesquisado ");
getchar();getchar();
return 0;
}
 
 
void mostrarUtente(tUtente utentes[], int *nutentes){
int i;
if (*nutentes==0){
    printf ("\nNao existem utentes para apresentar.\n");
}
else{
for (i=0;i<(*nutentes);i++){
    printf ("\n Numero: %d  Nome: %s  data de nascimento: %d-%d-%d\n", utentes[i].numero, utentes[i].nome, utentes[i].data.dia, utentes[i].data.mes, utentes[i].data.ano);
 
    }
}
getchar();getchar();
}
 
void eliminarUtente(tUtente utentes[], int *nUtentes)
{
    int numer, i, posicao=-1;
    if(*nUtentes==0){
        printf("\nNao existem utentes registados");
        getchar();getchar();
    }
    else{
        printf("\tInsira o numero do utente: ");
        scanf("%d", &numer);
        for(i=0; i<=*nUtentes; i++)
        {
            if(utentes[i].numero==numer){
                posicao=i;
            }
 
        }
        if(posicao>-1){
            for(i=posicao; i<*nUtentes-1; i++){
                utentes[i]=utentes[i+1];
            }
 
            *nUtentes-=1;
            printf("\n\nFoi eliminado com sucesso o utente");
            getchar();getchar();
        }
         else{
            printf("\n\nNao foi encontrada o Utente inserida");
            getchar();getchar();
        }
    }
 
}
 
 
//funções para os tratamentos----------------
 
void inserirTratamento(tTratamento tratamentos[], int *nTratamentos){
int i, numero;
 
if (*nTratamentos>=MAX_TRATAMENTO){
    printf("\n\tNao e possivel adicionar mais tratamentos. Elimine um antes de continuar");
    getchar();getchar();
}
else{
    printf("\n\tIndique o numero do novo tratamento: ");
        do{
            scanf("%d",&numero);
            for (i=0;i<*nTratamentos;i++){
                if(tratamentos[i].numero==numero){
                    printf("\nNumero ja existe. Insira outro numero: ");
                    break;
                }
 
            }
 
        }while(tratamentos[i].numero==numero);
        tratamentos[*nTratamentos].numero=numero;
 
        printf("\n\tIndique a designacao do novo tratamento: ");
        lerString(tratamentos[*nTratamentos].designacao, MAX_DESIGNACAO);
 
        printf("\n\tCusto do novo tratamento: ");
        scanf("%f" , &tratamentos[*nTratamentos].custo);
 
        (*nTratamentos)++;
 
    }
}
 
int alterarTratamento(tTratamento tratamentos[], int *nTratamentos, tClinica clinicas[], int *nClinicas){
 
int num_pesq, i, n, numero;
 
if (*nTratamentos==0){
    printf("\nNao existem tratamentos para poder alterar ");
    getchar();getchar();
    return 0;
}
 
printf("\nIndique o numero do tratamento que pretende alterar: ");
scanf("%d",&num_pesq);
 
for(i=0;i<*nTratamentos;i++){
    if (tratamentos[i].numero==num_pesq){
        printf("\n\n\tInsira o novo numero: ");
       // scanf("%d",&utentes[i].numero);
 
 
       do{
        scanf("%d",&numero);
        for (n=0;n<*nTratamentos;n++){
            if(tratamentos[n].numero==numero){
                printf("\nNumero ja existe. Insira outro numero: ");
                break;
            }
        }
 
    }while(tratamentos[n].numero==numero);
    tratamentos[i].numero=numero;
 
    printf("\n\tIndique a designacao do novo tratamento: ");
    lerString(tratamentos[i].designacao, MAX_DESIGNACAO);
 
    printf("\n\tCusto do novo tratamento: ");
    scanf("%f" , &tratamentos[i].custo);
 
    for(i=0; i<=*nClinicas; i++){
 
        for(n=0; n<=clinicas[i].ntratamentos; n++){
            if (clinicas[i].tratamentosDisponiveis[n]==num_pesq){
                clinicas[i].tratamentosDisponiveis[n]=numero;
            }
        }
 
    }
 
    return 0;
    }
 
}
printf("Nao foi encontrado o numero pesquisado ");
getchar();getchar();
return 0;
 
}
 
 
void mostrarTratamento(tTratamento tratamentos[], int *nTratamentos)
{
 int i;
 
    if (*nTratamentos==0){
        printf("\t\tNao existem tratamentos para apresentar\n\n");
    }
    else
    {
        for(i=0; i<*nTratamentos; i++)
        {
            printf("\n\tNumero: %d  Designacao: %s  Preco: %.2f \n", tratamentos[i].numero, tratamentos[i].designacao, tratamentos[i].custo);
        }
    }
 
getchar();getchar();
 
}
 
//funções para as clinicas---------
 
void inserirClinica(tClinica clinicas[], int *nClinica){
    int i;
    char design [MAX_DESIGNACAO];
    if (*nClinica>=MAX_CLINICA){
        printf("\n\tNao e possivel adicionar mais uma clinica. Elimine uma antes de continuar ");
        getchar();getchar();
    }
        else{
        printf("\n\tIndique a designacao da nova clinica: ");
 
        do{
            lerString(design, MAX_DESIGNACAO);
            for (i=0;i<*nClinica;i++){
                if(strcmp(clinicas[i].designacao,design)==0){
 
 
 
                    printf("\nClinica ja existe. Insira outro nome: ");
                    break;
                }
 
            }
 
        }while(strcmp(clinicas[i].designacao,design)==0);
        strcpy(clinicas[*nClinica].designacao,design);
 
 
        printf("\n\tLocalidade da nova clinica: ");
        lerString(clinicas[*nClinica].localidade, MAX_DESIGNACAO);
 
        clinicas[*nClinica].ntratamentos = 0;
 
        (*nClinica)++;
 
    }
}
int alterarClinica(tClinica clinicas[], int *nClinica){
int i, n;
char design_pesq [MAX_DESIGNACAO], design_comp[MAX_DESIGNACAO];
 
if (*nClinica==0){
    printf("\nNao existem clinicas para poder alterar ");
    getchar();getchar();
    return 0;
}
 
printf("\nIndique da clinica que pretende alterar: ");
lerString(design_pesq, MAX_DESIGNACAO);
 
for(i=0;i<*nClinica;i++){
    if (strcmp(clinicas[i].designacao,design_pesq)==0){
        printf("\n\n\tInsira o novo Nome: ");
        do{
        lerString(design_comp, MAX_DESIGNACAO);
        for (n=0;n<*nClinica;n++){
            if (strcmp(design_comp, clinicas[n].designacao)==0){
                printf("\nClinica ja existe. Insira outro nome: ");
                break;
            }
        }
 
    }while(strcmp(clinicas[n].designacao,design_comp)==0);
    strcpy(clinicas[i].designacao,design_comp);
 
    printf("\n\tLocalidade da nova clinica: ");
    lerString(clinicas[i].localidade, MAX_DESIGNACAO);
 
 
    return 0;
        }
    }
    printf("\n A clinica inserida nao existe ");
    getchar();getchar();
    return 0;
}
 
void eliminarClinica(tClinica clinicas[], int *nClinicas)
{
    int i, posicao=-1;
    char design[MAX_DESIGNACAO];
    if (*nClinicas==0){
        printf("\n\nNao existem Clinicas para eliminar ");
        getchar();getchar();
    }
    else{
 
        printf("\t\tIndique a designacao da clinica: ");
        lerString(design,MAX_DESIGNACAO);
        for(i=0; i<=*nClinicas; i++)
        {
            if(strcmp(clinicas[i].designacao,design)==0){
                posicao=i;
            }
        }
        if(posicao>-1)
        {
            for(i=posicao; i<*nClinicas-1; i++)
            {
                clinicas[i]=clinicas[i+1];
            }
            *nClinicas-=1;
            printf("\n\nFoi eliminada com sucesso a Clinica");
            getchar();getchar();
        }
        else{
            printf("\n\nNao foi encontrada a Clinica inserida");
            getchar();getchar();
        }
    }
}
 
void mostrarClinica(tClinica clinicas[], int *nClinicas)
{
    int i;
 
    if (*nClinicas==0){
        printf("\t\tNao existem clinicas para apresentar\n\n");
    }
    else
    {
        for(i=0; i<*nClinicas; i++)
        {
            printf("\n\tDesignacao: %s  Localidade: %s  Nr tratamentos disponiveis: %d \n", clinicas[i].designacao, clinicas[i].localidade, clinicas[i].ntratamentos);
        }
    }
 
getchar();getchar();
}
 
int associarTratamentos (tClinica clinicas[], int *nClinicas, tTratamento tratamentos[], int *nTratamentos){
 
char nome_clinica_pesquisa [MAX_DESIGNACAO], nome_tratamento_pesquisa [MAX_DESIGNACAO];
 
int i, n, k, posicao_clinica=-1, posicao_tratamento=-1;
 
 
//verificações iniciais para saber se existem clinicas e tratamentos para associar
 
    char design[MAX_DESIGNACAO];
    if (*nClinicas==0){
        printf("\n\nNao existem Clinicas para associar ");
        getchar();getchar();
        return 0;
    }
 
 
    if (*nTratamentos==0){
    printf("\n\nNao existem Tratamentos para asssociar ");
    getchar();getchar();
    return 0;
    }
 
//pedido e validação da clinica
    printf("Indique o nome da clinica a qual pretende associar tratamentos: ");
    lerString(nome_clinica_pesquisa,MAX_DESIGNACAO);
 
    for(i=0; i<=*nClinicas; i++){
        if(strcmp(clinicas[i].designacao,nome_clinica_pesquisa)==0){
            //foi encontrada a clinica. é entao armazenado a posição e de seguida é pedido o tratamento
        //    printf ("clinica encontrada");
            posicao_clinica=i;
 
            //validação para saber se o limite de tratamentos ainda nao foi alcançado
            if (clinicas[posicao_clinica].ntratamentos>=MAX_TRATAMENTOS_DISPONIVEIS){
            printf("\n\nNao e possivel associar mais tratamentos a essa clinica ");
            getchar();getchar();
            return 0;
            }
 
//pedir e validação do tratamento
            printf("\nIndique o nome do tratamento que pretende associar: ");
            lerString(nome_tratamento_pesquisa,MAX_DESIGNACAO);
 
            for(n=0; n<=*nTratamentos; n++){
                if(strcmp(tratamentos[n].designacao,nome_tratamento_pesquisa)==0){
                    //tratamento encontrado. é guardado a posição para mais tarde ser usada para associar a clinica
                //    printf ("tratamento encontrado");
                    posicao_tratamento=n;
 
                    //verificar se o tratamento nao se encontra ja associado
 
                    for (k=0; k<=clinicas[posicao_clinica].ntratamentos;k++){
                        if (clinicas[posicao_clinica].tratamentosDisponiveis[k]==tratamentos[posicao_tratamento].numero){
                            printf("\n\tTratamento ja esta associado ");
                            getchar();getchar();
                            return 0;
                        }
                    }
 
 
                    //associar tratamento e clinica
                    clinicas[posicao_clinica].tratamentosDisponiveis[clinicas[posicao_clinica].ntratamentos]=tratamentos[posicao_tratamento].numero;
                    printf("Associacao concluida com sucesso ");
                    clinicas[posicao_clinica].ntratamentos+=1;
                    getchar();getchar();
                    return 0;
                }
            }
//fim da procura do tratamento. se chegou aqui é porque nao existe
            printf("Nao foi encontrado o tratamento inserido ");
            getchar();getchar();
            return 0;
        }
    }
 
//fim da procura da clinica. se chegou aqui é porque nao existe
 
    printf("Nao foi encontrada a Clinica inserida ");
    getchar();getchar();
    return 0;
}
 
int mostrarTratamentosClinica (tClinica clinicas[], int *nClinicas, tTratamento tratamentos[], int *nTratamentos){
    char nome_clinica_pesquisa [MAX_DESIGNACAO], nome_tratamento_pesquisa [MAX_DESIGNACAO];
    int i, n, k, posicao_clinica=-1, posicao_tratamento=-1;
 
 
    if (*nClinicas==0){
        printf("\n\nNao existem Clinicas para mostrar ");
        getchar();getchar();
        return 0;
    }
 
    printf("Indique o nome da clinica da qual pretende mostrar os tratamentos: ");
    lerString(nome_clinica_pesquisa,MAX_DESIGNACAO);
    for(i=0; i<=*nClinicas; i++){
        if(strcmp(clinicas[i].designacao,nome_clinica_pesquisa)==0){
            //foi encontrada a clinica. é entao armazenado a posição e de seguida é pedido o tratamento
            posicao_clinica=i;
 
            printf("\nNumero de tratamentos disponiveis: %d\n",clinicas[posicao_clinica].ntratamentos);
            for (i=0; i<clinicas[posicao_clinica].ntratamentos;i++){
                printf("\n tratamento: %d", clinicas[posicao_clinica].tratamentosDisponiveis[i]);
 
                for (k=0;k<=*nTratamentos;k++){
                if(tratamentos[k].numero==clinicas[posicao_clinica].tratamentosDisponiveis[i]){
 
                printf(" - %s \n\n",tratamentos[k].designacao );
                }
                }
            }
        }
    }
    getchar();getchar();
    return 0;
}
 
//ficheiros---------------------------------------------------------
 
//utentes
 
void escreverFicheiroBinarioUtente(tUtente utentes[], int nUtentes)
{
 
    FILE *trabalhoU;
    trabalhoU=fopen("Utentes.data","wb");
 
    if(trabalhoU==NULL)
    {
        printf("\t\t\tERRO ao abrir ficheiro de escrever Utentes!\n\n");
    }
    else
    {
        fwrite(&nUtentes, sizeof(int), 1, trabalhoU);
        fwrite(utentes, sizeof(tUtente), nUtentes, trabalhoU);
        fclose(trabalhoU);
    }
 
 
}
 
 
void lerFicheiroBinarioUtente(tUtente utentes[], int *nUtentes)
{
 
    FILE *trabalhoU;
    int lol;
 
    trabalhoU=fopen("Utentes.data", "rb");
 
    if (trabalhoU==NULL)
    {
        printf("\t\t\tERRO ao abrir ficheiro ler Utentes!!\n\n");
    }
    else
    {
        fread(nUtentes, sizeof(int), 1, trabalhoU);
        lol=fread(utentes, sizeof(tUtente), *nUtentes, trabalhoU);
 
        if (lol!=(*nUtentes))
        {
            (*nUtentes)=lol;
 
        }
        fclose(trabalhoU);
    }
 
}
 
 
 
//clinicas
 
void escreverFicheiroBinarioClinica(tClinica clinicas[],int nClinicas){
 
FILE *trabalhoC;
trabalhoC=fopen("Clinicas.data", "wb");
 
if (trabalhoC==NULL)
{
    printf("\t\t\tERRO ao abrir ficheiro escrever Clinicas\n\n");
}
else{
    fwrite(&nClinicas, sizeof(int), 1, trabalhoC);
    fwrite(clinicas, sizeof(tClinica), nClinicas, trabalhoC);
    fclose(trabalhoC);
}
 
}
 
 
void lerFicheiroBinarioClinica(tClinica clinicas[],int *nClinicas){
 
    FILE *trabalhoC;
    int lol;
 
    trabalhoC=fopen("clinicas.data", "rb");
 
    if (trabalhoC==NULL)
    {
        printf("\t\t\tERRO ao abrir ficheiro ler Clinicas!!\n\n");
    }
    else
    {
        fread(nClinicas, sizeof(int), 1, trabalhoC);
        lol=fread(clinicas, sizeof(tClinica), *nClinicas, trabalhoC);
 
        if (lol!=(*nClinicas))
        {
            (*nClinicas)=lol;
 
        }
        fclose(trabalhoC);
    }
 
}
 
//tratamentos
 
void escreverFicheiroBinarioTratamento(tTratamento tratamentos[],int nTratamentos){
 
FILE *trabalhoT;
trabalhoT=fopen("Tratamentos.data", "wb");
 
if (trabalhoT==NULL)
{
    printf("\t\t\tERRO ao abrir ficheiro escrever Tratamentos\n\n");
}
else{
    fwrite(&nTratamentos, sizeof(int), 1, trabalhoT);
    fwrite(tratamentos, sizeof(tTratamento), nTratamentos, trabalhoT);
    fclose(trabalhoT);
}
 
}
 
 
void lerFicheiroBinarioTratamento(tTratamento tratamentos[],int *nTratamentos){
 
    FILE *trabalhoT;
    int lol;
 
    trabalhoT=fopen("Tratamentos.data", "rb");
 
    if (trabalhoT==NULL)
    {
        printf("\t\t\tERRO ao abrir ficheiro ler Tratamentos!!\n\n");
    }
    else
    {
        fread(nTratamentos, sizeof(int), 1, trabalhoT);
        lol=fread(tratamentos, sizeof(tTratamento), *nTratamentos, trabalhoT);
 
        if (lol!=(*nTratamentos))
        {
            (*nTratamentos)=lol;
 
        }
        fclose(trabalhoT);
    }
 
}